//
//  UIBaseApplication.swift
//  Belajar
//
//  Created by Ebizu on 11/16/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

protocol UIBaseAppComponent {
    
    func initView()
    
    func initSpinner()
    
    func initProperties()
    
    func registerCell(_ nibName: String, _ bundle: Bundle?, _ cellIdentifier: String)
    
    func registerCell(_ nibName: String, _ cellIdentifier: String)
    
    func registerCell(_ nibName: String)
    
    func addSection(_ cellIdentifier: String, _ sectionName: String, _ items: [EntityDelegate])
    
    func addSection(_ cellIdentifier: String, _ sectionName: String)
    
    func addSectionItems(_ sectionName: String, _ items: [EntityDelegate])
    
    func updateCell(_ section: Int, _ startIndex:Int, _ lastIndex:Int)
    
    func showErrorMessageDialog(_ message: String )
    
    func loadable(_ bool: Bool)
    
    func showIndicator()
    
    func onPullRefresh()
    
    func onLoadMoreIndicator()
    
    func stopPullRefresh()
    
    func stopLoadMoreIndicator()
    
    func stopAllIndicator()
    
}

