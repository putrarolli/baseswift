//
//  UIBaseCollectionViewDelegate.swift
//  Belajar
//
//  Created by Zuliadin on 19/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

protocol UIBaseCollectionViewDelegate {
    
    func sizeForItemAt(indexPath: IndexPath) -> CGSize
    func insetForSectionAt(section: Int) -> UIEdgeInsets
    
}
