//
//  UIBaseTableSectionModel.swift
//  Belajar
//
//  Created by Ebizu on 10/18/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation

class UIBaseSection {
    
    var cellIdentifier: String = ""
    var sections: String  = ""
    var items:[EntityDelegate] = [EntityDelegate]()

    init() {
        
    }
    
    init(_ cellIdentifier: String, _ sections: String,_ items: [EntityDelegate]) {
        self.cellIdentifier = cellIdentifier
        self.sections = sections
        self.items = items
    }
    
}
