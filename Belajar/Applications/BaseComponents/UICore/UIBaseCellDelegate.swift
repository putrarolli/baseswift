//
//  UIBaseCell.swift
//  Belajar
//
//  Created by Zuliadin on 18/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

protocol UIBaseCellDelegate {
    func setupCell(_ value: EntityDelegate, _ indexPath: IndexPath)
    func setupCell(_ value: EntityDelegate)
    func onSelected(_ value: EntityDelegate, _ indexPath: IndexPath)
    func onSelected(_ value: EntityDelegate)
}
