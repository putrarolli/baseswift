//
//  BaseUITableViewCell.swift
//  Belajar
//
//  Created by Ebizu on 10/18/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

class UIBaseTableViewCell: UITableViewCell {
    
    var delegate: UIBaseCellDelegate?
    
    func setupCell(_ value: EntityDelegate) {
        delegate?.setupCell(value)
    }
    
    func setupCell(_ value: EntityDelegate, _ indexPath: IndexPath) {
        delegate?.setupCell(value, indexPath)
    }
    
    func onSelected(_ value: EntityDelegate) {
        delegate?.onSelected(value)
    }
    
    func onSelected(_ value: EntityDelegate, _ indexPath: IndexPath) {
        delegate?.onSelected(value, indexPath)
    }
    
}

