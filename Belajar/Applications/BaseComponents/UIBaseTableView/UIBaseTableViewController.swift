//
//  BaseUITableView.swift
//  Belajar
//
//  Created by Ebizu on 10/18/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

class UIBaseTableViewController: UIViewController, UIBaseAppComponent {
    
    var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    var sections :[UIBaseSection] = [UIBaseSection]();
    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)
    var heightAtIndexPath = NSMutableDictionary()
    var delegate: UIBaseCellDelegate?
    private var isLoadable = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initSpinner()
        initProperties()
    }
    
    func initView() {
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
    }
    
    func initSpinner() {
        self.spinner.startAnimating()
        self.spinner.color = UIColor.red
        self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
    }
    
    func initProperties() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.view.addSubview(tableView)
        self.tableView.allowsSelection = true
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(onPullRefresh), for: .valueChanged)
        self.refreshControl.tintColor = UIColor.red
        self.refreshControl.tag = 100
        loadable(true)
    }
    
    func loadable(_ bool: Bool) {
        isLoadable = bool
        if !bool {
            self.tableView.tableFooterView = nil
            self.refreshControl.removeTarget(nil, action: nil, for: .allEvents)
            if let viewWithTag = self.tableView.viewWithTag(100) {
                viewWithTag.removeFromSuperview()
            }
        } else {
            self.tableView.tableFooterView = spinner
            self.tableView.addSubview(refreshControl)
            let pinTop = NSLayoutConstraint(item: spinner, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 100)
            self.spinner.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([pinTop])
        }
    }
    
    func onPullRefresh() {
        
    }
    
    func stopPullRefresh() {
        if self.refreshControl.isRefreshing{
            self.refreshControl.endRefreshing()
        }
    }
    
    func registerCell(_ nibName: String, _ bundle: Bundle?, _ cellIdentifier: String) {
        self.tableView.register(UINib(nibName: nibName, bundle: bundle), forCellReuseIdentifier: cellIdentifier)
    }
    
    func registerCell(_ nibName: String, _ cellIdentifier: String) {
        self.registerCell(nibName, nil, cellIdentifier)
    }
    
    func registerCell(_ nibName: String) {
        self.registerCell(nibName, nil, nibName)
    }
    
    func addSection(_ cellIdentifier: String, _ sectionName: String) {
        sections.append(UIBaseSection(cellIdentifier, sectionName, [EntityDelegate]()))
    }
    
    func addSection(_ cellIdentifier: String, _ sectionName: String, _ items: [EntityDelegate]) {
        sections.append(UIBaseSection(cellIdentifier, sectionName, items))
    }
    
    func addSectionItems(_ sectionName: String, _ items: [EntityDelegate]) {
        for (index, section) in sections.enumerated() {
            if sectionName == section.sections {
                let startIndex: Int = section.items.count
                section.items.append(contentsOf: items)
                updateCell(index, startIndex, section.items.count - 1)
                break
            }
        }
    }
    
    func updateCell(_ section: Int, _ startIndex:Int, _ lastIndex:Int) {
        if 0 == startIndex {
            self.tableView.reloadData()
        } else {
            UIView.performWithoutAnimation({
                self.tableView.beginUpdates()
                for i in startIndex ... (lastIndex) {
                    self.tableView.insertRows(at:[IndexPath(row:(i), section:section)], with: .none )
                }
                self.tableView.endUpdates()
            })
        }
    }
    
    func showErrorMessageDialog(_ message: String ){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Close", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
   
    func stopLoadMoreIndicator(){
        if isLoadable { self.tableView.tableFooterView?.isHidden = true }
    }
    
    func showIndicator(){
        if isLoadable { self.tableView.tableFooterView?.isHidden = false ; onLoadMoreIndicator()}
    }
    
    func stopAllIndicator() {
        self.stopPullRefresh()
        self.stopLoadMoreIndicator()
    }
    
    func onLoadMoreIndicator() {
        
    }
    
}

extension UIBaseTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.sections[indexPath.section].cellIdentifier, for: indexPath) as? UIBaseTableViewCell {
            cell.setupCell(self.sections[indexPath.section].items[indexPath.row])
            cell.setupCell(self.sections[indexPath.section].items[indexPath.row], indexPath)
            delegate?.setupCell(self.sections[indexPath.section].items[indexPath.row])
            delegate?.setupCell(self.sections[indexPath.section].items[indexPath.row], indexPath)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = self.heightAtIndexPath.object(forKey: indexPath)
        if (nil != (height)) {
            return CGFloat(height as! CGFloat)
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = cell.frame.size.height
        self.heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.sections[indexPath.section].cellIdentifier, for: indexPath) as? UIBaseTableViewCell {
            cell.onSelected(self.sections[indexPath.section].items[indexPath.row])
            cell.onSelected(self.sections[indexPath.section].items[indexPath.row], indexPath)
            delegate?.onSelected(self.sections[indexPath.section].items[indexPath.row])
            delegate?.onSelected(self.sections[indexPath.section].items[indexPath.row], indexPath)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.frame.size.height + scrollView.contentOffset.y >= scrollView.contentSize.height) {
            self.showIndicator()
        }
    }
    
}
