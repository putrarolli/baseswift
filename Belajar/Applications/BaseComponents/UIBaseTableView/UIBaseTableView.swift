//
//  UIBaseTableView.swift
//  Belajar
//
//  Created by Ebizu on 10/18/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

class UIBaseTableView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    var sections :[UIBaseSection] = [UIBaseSection]();
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("init base \(self.frame.height)")
        initialize()
        registerTableCell()
        addSectionAndItems()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        print("init frame")
    }
    
    func initialize() {
        // self.backgroundColor = UIColor.blue
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: self.frame.height))
        tableView.translatesAutoresizingMaskIntoConstraints = true
        self.tableView.layer.backgroundColor = UIColor.black.cgColor
        self.addSubview(tableView)
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.refreshControl.tintColor = UIColor.red
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = refreshControl
        } else {
            self.tableView.addSubview(refreshControl)
        }
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func refresh(param:Any) {
        self.loadStockQuoteItems()
    }
    
    func loadStockQuoteItems() {
            if self.refreshControl.isRefreshing{
                self.refreshControl.endRefreshing()
            }
            self.tableView?.reloadData()
    }
    
    func registerTableCell() {}
    
    func addSectionAndItems() {}
    
    func registerCell(_ nibName: String, _ bundle: Bundle?, _ cellIdentifier: String) {
        self.tableView.register(UINib(nibName: nibName, bundle: bundle), forCellReuseIdentifier: cellIdentifier)
    }
    
    func registerCell(_ nibName: String, _ cellIdentifier: String) {
        self.registerCell(nibName, nil, cellIdentifier)
    }
    
    func registerCell(_ nibName: String) {
        self.registerCell(nibName, nil, nibName)
    }
    
    func addSection(_ cellIdentifier: String, _ sectionName: String, _ items: [EntityDelegate]) {
        sections.append(UIBaseSection(cellIdentifier, sectionName, items))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.sections[indexPath.section].cellIdentifier, for: indexPath) as? UIBaseTableViewCell {
            // print("\(self.sections[indexPath.section].items[indexPath.row])")
            cell.setupCell(self.sections[indexPath.section].items[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

extension UIRefreshControl {
    func refreshManually() {
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: false)
        }
        beginRefreshing()
        sendActions(for: .valueChanged)
    }
}
