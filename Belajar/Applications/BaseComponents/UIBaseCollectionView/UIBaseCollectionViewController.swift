//
//  UIBaseCollectionViewController.swift
//  Belajar
//
//  Created by Ebizu on 11/16/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

class UIBaseCollectionViewController: UIViewController, UIBaseAppComponent {
    
    var collectionView: UICollectionView!
    var refreshControl = UIRefreshControl()
    var sections :[UIBaseSection] = [UIBaseSection]();
    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)
    var heightAtIndexPath = NSMutableDictionary()
    var delegate: UIBaseCellDelegate?
    var delegateCollection: UIBaseCollectionViewDelegate?
    
    private var isLoadable = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initSpinner()
        initCells()
        initProperties()
    }
    
    func initView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        self.collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        
    }
    
    func initSpinner() {
        self.spinner.startAnimating()
        self.spinner.color = UIColor.red
        self.spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: collectionView.bounds.width, height: CGFloat(44))
    }
    
    func initProperties() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = UIColor.white
        self.view.addSubview(collectionView)
        self.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellBasic")
    }
    
    func initCells(){
        
    }
    
    func registerCell(_ nibName: String, _ bundle: Bundle?, _ cellIdentifier: String) {
        self.collectionView.register(UINib(nibName : nibName, bundle : bundle), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    func registerCell(_ nibName: String, _ cellIdentifier: String) {
        self.registerCell(nibName, nil, cellIdentifier)
    }
    
    func registerCell(_ nibName: String) {
        self.registerCell(nibName, nil, nibName)
    }
    
    func addSection(_ cellIdentifier: String, _ sectionName: String) {
        sections.append(UIBaseSection(cellIdentifier, sectionName, [EntityDelegate]()))
    }
    
    func addSection(_ cellIdentifier: String, _ sectionName: String, _ items: [EntityDelegate]) {
        sections.append(UIBaseSection(cellIdentifier, sectionName, items))
    }
    
    func addSectionItems(_ sectionName: String, _ items: [EntityDelegate]) {
        if !items.isEmpty {
            // get section
            for (index, section) in sections.enumerated() {
                if sectionName == section.sections {
                    let start = 0 < section.items.count ? section.items.count - 1 : 0
                    let end = start + (items.count - 1)
                    var indexPats: [IndexPath] = [IndexPath]()
                    for i in start ... end {
                        indexPats.append(IndexPath(row: i, section: index))
                    }
                    self.collectionView.performBatchUpdates({() -> Void in
                        section.items.append(contentsOf: items)
                        self.collectionView.insertItems(at: indexPats)
                    }, completion: {(Bool) -> Void in
                        
                    })
                }
            }
        }
    }
    
    func updateCell(_ section: Int, _ startIndex: Int, _ lastIndex: Int) {
        if 0 == startIndex {
            //self.tableView.reloadData()
        } else {
            UIView.performWithoutAnimation({
                /*
                 self.tableView.beginUpdates()
                 for i in startIndex ... (lastIndex) {
                 self.tableView.insertRows(at:[IndexPath(row:(i), section:section)], with: .none )
                 }
                 self.tableView.endUpdates()
                 */
            })
        }
    }
    
    func showErrorMessageDialog(_ message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Close", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func loadable(_ bool: Bool) {
        isLoadable = bool
        if !bool {
            //self.collectionView.tableFooterView = nil
            self.refreshControl.removeTarget(nil, action: nil, for: .allEvents)
            if let viewWithTag = self.collectionView.viewWithTag(100) {
                viewWithTag.removeFromSuperview()
            }
        } else {
            //self.collectionView.tableFooterView = spinner
            self.collectionView.addSubview(refreshControl)
            let pinTop = NSLayoutConstraint(item: spinner, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 100)
            self.spinner.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([pinTop])
        }
    }
    
    func showIndicator() {
        // if isLoadable { self.tableView.tableFooterView?.isHidden = false ; onLoadMoreIndicator()}
    }
    
    func onPullRefresh() {
        
    }
    
    func onLoadMoreIndicator() {
        
    }
    
    func stopPullRefresh() {
        if self.refreshControl.isRefreshing{
            self.refreshControl.endRefreshing()
        }
    }
    
    func stopLoadMoreIndicator() {
        // if isLoadable { self.tableView.tableFooterView?.isHidden = true }
    }
    
    func stopAllIndicator() {
        self.stopPullRefresh()
        self.stopLoadMoreIndicator()
    }
    
}

extension UIBaseCollectionViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return UICollectionReusableView()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sections[section].items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.sections[indexPath.section].cellIdentifier, for: indexPath) as? UIBaseCollectionViewCell {
            cell.setupCell(self.sections[indexPath.section].items[indexPath.row])
            cell.setupCell(self.sections[indexPath.section].items[indexPath.row], indexPath)
            delegate?.setupCell(self.sections[indexPath.section].items[indexPath.row])
            delegate?.setupCell(self.sections[indexPath.section].items[indexPath.row], indexPath)
            return cell
        } else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: self.sections[indexPath.section].cellIdentifier, for: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let size = delegateCollection {
            return size.sizeForItemAt(indexPath: indexPath)
        }
        return CGSize(width: collectionView.frame.size.width, height: 80)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if let size = delegateCollection {
            return size.insetForSectionAt(section:section)
        }
        return UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 10)
    }
    
}
