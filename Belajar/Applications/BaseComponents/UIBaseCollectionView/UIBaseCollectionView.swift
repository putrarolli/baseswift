//
//  UIBaseCollectionView.swift
//  Belajar
//
//  Created by Zuliadin on 18/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

class UIBaseCollectionView: UIView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    var collectionView: UICollectionView!
    var refreshControl = UIRefreshControl()
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    var sections :[UIBaseSection] = [UIBaseSection]();
    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)
    var heightAtIndexPath = NSMutableDictionary()
    var delegate: UIBaseCellDelegate?
    //
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    // size property
    var cellSize: CGSize?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("init base \(self.frame.height)")
        initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        print("init frame")
        initialize()
    }
    
    func initialize() {
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        // layout.itemSize = CGSize(width: 60, height: 60)
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: self.frame.height), collectionViewLayout: layout)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = UIColor.white
        self.addSubview(self.collectionView)
    }
    
    func registerCell(_ nibName: String, _ bundle: Bundle?, _ cellIdentifier: String) {
        self.collectionView.register(UINib(nibName : nibName, bundle : bundle), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    func registerCell(_ nibName: String, _ cellIdentifier: String) {
        self.registerCell(nibName, nil, cellIdentifier)
    }
    
    func registerCell(_ nibName: String) {
        self.registerCell(nibName, nil, nibName)
    }
    
    func addSection(_ cellIdentifier: String, _ sectionName: String) {
        sections.append(UIBaseSection(cellIdentifier, sectionName, [EntityDelegate]()))
    }
    
    func addSection(_ cellIdentifier: String, _ sectionName: String, _ items: [EntityDelegate]) {
        sections.append(UIBaseSection(cellIdentifier, sectionName, items))
    }
    
    func addSectionItems(_ sectionName: String, _ items: [EntityDelegate]) {
        if !items.isEmpty {
            // get section
            for (index, section) in sections.enumerated() {
                if sectionName == section.sections {
                    let start = 0 < section.items.count ? section.items.count - 1 : 0
                    let end = start + (items.count - 1)
                    var indexPats: [IndexPath] = [IndexPath]()
                    for i in start ... end {
                        indexPats.append(IndexPath(row: i, section: index))
                    }
                    self.collectionView.performBatchUpdates({() -> Void in
                        section.items.append(contentsOf: items)
                        self.collectionView.insertItems(at: indexPats)
                    }, completion: {(Bool) -> Void in
                        
                    })
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return UICollectionReusableView()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sections[section].items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.sections[indexPath.section].cellIdentifier, for: indexPath) as? UIBaseCollectionViewCell {
            cell.setupCell(self.sections[indexPath.section].items[indexPath.row])
            cell.setupCell(self.sections[indexPath.section].items[indexPath.row], indexPath)
            delegate?.setupCell(self.sections[indexPath.section].items[indexPath.row])
            delegate?.setupCell(self.sections[indexPath.section].items[indexPath.row], indexPath)
            return cell
        } else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: self.sections[indexPath.section].cellIdentifier, for: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let size = cellSize {
            return size
        }
        return CGSize(width: screenWidth, height: 80);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 10)
    }
    
    
}
