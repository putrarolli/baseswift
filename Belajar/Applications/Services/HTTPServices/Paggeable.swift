//
//  HttpRequestPage.swift
//  Manis
//
//  Created by Ebizu on 9/29/17.
//  Copyright © 2017 EBIZU. All rights reserved.
//
//  Belum bisa generic parameter
//  issue :  beberapa parameter tetap dikirim dengan value nil

import Foundation

class HttpRequestPage {
    
    var action:String! = ""
    var type:String! = ""
    var page:String! = "0"
    var limit:String! = "10"
    
    init(_ action: String, _ page: String, _ limit: String) {
        self.action = action
        self.page = page
        self.limit = limit
    }
    
    init(_ action: String, _ type: String, _ page: String, _ limit: String) {
        self.action = action
        self.type = type
        self.page = page
        self.limit = limit
    }
    
    func incrementPage() {
        let pageTmp:Int = Int(self.page)!
        self.page = String(pageTmp + 1)
    }
    
    func decrementPage() {
        let pageTmp:Int = Int(self.page)!
        if 0 < pageTmp {
            self.page = String(pageTmp - 1)
        }
    }
    
    func params() -> [String: Any] {
        return [
            "action" : self.action,
            "type" : self.type,
            "limit" : self.limit,
            "page" : self.page
        ]
    }
}
