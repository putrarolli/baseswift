//
//  AlmofireService.swift
//  AlmofireExample
//
//  Created by Ebizu on 8/23/17.
//  Copyright © 2017 Ebizu. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

typealias RequestCompleted<T:Mappable> = (T) -> ()
typealias RequestError = (Int, String) -> ()

protocol AlmofireServiceDelegate {
    func onErrorRequest(errorCode: Int, errorMessage: String)
}

class AlmofireService {
    
    private var isRequest = false
    
    /*!
     JSONEncoding.default | JSONEncoding.default
     */
    private func request<T:Mappable>(_ endPoint: String!, method: HTTPMethod, parameters: Parameters, _ completed: RequestCompleted<T>?, _ errorHandler: RequestError?, _ mainThread:Bool) {
        self.isRequest = true
        Alamofire.request(endPoint, method: .post, parameters:  parameters, encoding: URLEncoding.default).responseJSON { response in
            // self.printDebug(response)
            self.isRequest = false
            if response.result.isSuccess {
                if let o = Mapper<T>().map(JSONObject: response.value) {
                    if mainThread {
                        DispatchQueue.main.async { completed?(o) }
                    } else {
                        completed?(o)
                    }
                } else {
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        errorHandler?(700, "Cannot convert response to object! \(utf8Text)")
                    } else {
                        errorHandler?(701, "Please debug for error response!")
                    }
                }
                
            } else {
                if nil != response.response {
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        print("Data: \(utf8Text)")
                        errorHandler?((response.response?.statusCode)!, utf8Text)
                    } else {
                        errorHandler?(701, "Please debug for error response!")
                    }
                } else {
                    errorHandler?(703, "Request failure cannot get response")
                }
            }
        }
    }
    
    /*!
     JSONEncoding.default | JSONEncoding.default
     */
    private func requestObject<T:Mappable>(_ endPoint: String!, method: HTTPMethod, parameters: Parameters, _ completed: RequestCompleted<T>?, _ errorHandler: RequestError?, _ mainThread:Bool) {
        self.isRequest = true
        Alamofire.request(endPoint, method: .post, parameters:  parameters, encoding: JSONEncoding.default).responseObject{(response : DataResponse<T>) in
            self.isRequest = false
            // dump(response)
            if response.result.isSuccess {
                if let o = Mapper<T>().map(JSONObject: response.value) {
                    if mainThread {
                        DispatchQueue.main.async { completed?(o) }
                    } else {
                        completed?(o)
                    }
                } else {
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        errorHandler?(-200, "Cannot convert response to object! \(utf8Text)")
                    } else {
                        errorHandler?(-200, "Please debug for error response!")
                    }
                }
            } else {
                if nil != response.response {
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        print("Data: \(utf8Text)")
                        errorHandler?((response.response?.statusCode)!, utf8Text)
                    } else {
                        errorHandler?(-200, "Please debug for error response!")
                    }
                } else {
                    errorHandler?(-200, "Request failure")
                }
            }
        }
    }
    
    private func printDebug(_ response: Alamofire.DataResponse<Any>) {
        print("Response Code: \(String(describing: response.response?.statusCode))")
        print("Request: \(String(describing: response.request))")   // original url request
        print("Response: \(String(describing: response.response))") // http url response
        print("Result: \(response.result)")                         // response serialization result
        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
            print("Data: \(utf8Text)")
        }
    }
    
    func onRequest() -> Bool {
        return isRequest
    }
    
    func requestSyncs<T:Mappable>(_ endPoint: String!, parameters: [String: Any]!, _ completed: RequestCompleted<T>?, _ errorHandler: RequestError?) {
    }
    func requestSync<T:Mappable>(_ endPoint: String!, parameters: [String: Any]!, _ completed: RequestCompleted<T>?, _ errorHandler: RequestError?) {
        self.request(endPoint, method: HTTPMethod.post, parameters: parameters, completed, errorHandler, true)
    }
    
    func requestAsync<T:Mappable>(_ endPoint: String!, parameters: [String: Any]!, _ completed: RequestCompleted<T>?, _ errorHandler: RequestError?) {
        self.request(endPoint, method: HTTPMethod.post, parameters: parameters, completed, errorHandler, false)
    }
    
}
