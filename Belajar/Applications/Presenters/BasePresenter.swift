//
//  BasePresenter.swift
//  Belajar
//
//  Created by Ebizu on 10/20/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation

class BasePresenter {
    
    let service = AlmofireService()
    var page = 1
    
    func resetPage() {
        page = 1
    }
    
    func incrementPage() {
        self.page = (self.page) + 1
    }
    
    func resetPage(_ pageCurrent: inout Int) {
        pageCurrent = 1
    }
    
    func incrementPage(_ pageCurrent: inout Int) {
        pageCurrent = pageCurrent + 1
    }
    
    func pageLimit() -> Int {
        return (0 == page ? 20 : 10)
    }
    
}
