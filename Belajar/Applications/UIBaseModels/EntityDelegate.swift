//
//  BaseEntityDelegate.swift
//  Belajar
//
//  Created by Ebizu on 10/19/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation

protocol EntityDelegate {
    
    func returnEntity() -> Self;
    
}
