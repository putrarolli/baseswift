//
//  ResponseDTO.swift
//  Belajar
//
//  Created by Ebizu on 10/20/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseDTO<T:Mappable>: Mappable {
    
    var error_code: String?
    var error_msg: String?
    var user: String?
    var data:[T]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        error_code <- map["error_code"]
        error_msg <- map["error_msg"]
        user <- map["user"]
        data <- map["data"]
    }
    
}

