//
//  CiseModel.swift
//  SamplePagging001
//
//  Created by Ebizu on 10/4/17.
//  Copyright © 2017 Ebizu. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseEntity: Mappable {
    
    init(){
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {}
}
