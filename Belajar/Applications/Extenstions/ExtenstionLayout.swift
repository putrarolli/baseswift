//
//  ExtenstionLayout.swift
//  Belajar
//
//  Created by Ebizu on 10/23/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

extension NSLayoutConstraint {
    
    override open var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}
