//
//  ExtenstionsPrimitif.swift
//  Belajar
//
//  Created by Ebizu on 10/19/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation

extension Int: EntityDelegate {
    
    func returnEntity() -> Int {
        return self
    }
}

extension UInt: EntityDelegate {
    
    func returnEntity() -> UInt {
        return self
    }
}

extension Float: EntityDelegate {
    
    func returnEntity() -> Float {
        return self
    }
}

extension Double: EntityDelegate {
    
    func returnEntity() -> Double {
        return self
    }
}

extension Bool: EntityDelegate {
    
    func returnEntity() -> Bool {
        return self
    }
}

extension Character: EntityDelegate {
    
    func returnEntity() -> Character {
        return self
    }
}

extension Optional: EntityDelegate {
    
    func returnEntity() -> Optional {
        return self
    }
}

extension String: EntityDelegate {
    
    func returnEntity() -> String {
        return self
    }
}

extension BaseEntity: EntityDelegate {
    
    func returnEntity() -> Self {
        return self
    }
}
