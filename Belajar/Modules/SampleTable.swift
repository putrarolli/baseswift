//
//  SampleTable.swift
//  Belajar
//
//  Created by Ebizu on 10/18/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

class SampleTable: UIBaseTableView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("init")
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func registerTableCell() {
        self.registerCell("ListTableViewCell")
    }
    
    override func addSectionAndItems() {
        self.addSection("ListTableViewCell", "One2Three", ["T1","T2","T3"])
        
    }
}
