//
//  SplashVC.swift
//  Belajar
//
//  Created by putra rolli on 9/24/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit

class SplashVC: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = UIColor.brown
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.splashTimeOut(sender:)), userInfo: nil, repeats: false)
    }
    
    
    func splashTimeOut(sender: Timer){
        // self.show(HomeVC(), sender: sender)
    }
    
}
