//
//  TableViewCollectionCell.swift
//  Belajar
//
//  Created by Ebizu on 10/18/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit

class TableViewCollectionCell: UIBaseTableViewCell {
    
    @IBOutlet weak var sampleTable: SampleTable!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
