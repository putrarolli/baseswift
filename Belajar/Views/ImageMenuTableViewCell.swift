//
//  ImageMenuTableViewCellTableViewCell.swift
//  Belajar
//
//  Created by putra rolli on 9/24/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit

class ImageMenuTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var label_text: UILabel!
    
    var fruits = "Apple"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        label_text.text = fruits
    }

    override func setupCell(_ value: EntityDelegate) {
        label_text.text = "\(value)"
    }
    
}
