//
//  ListTableViewCell.swift
//  Belajar
//
//  Created by putra rolli on 9/24/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit

class ListTableViewCell: UIBaseTableViewCell {

    var fruits = "Manggo"
    
    @IBOutlet weak var label_text: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        label_text.text = fruits
    }
    
    override func setupCell(_ value: EntityDelegate) {
        if let resource = value as? Resource{
            label_text.text = resource.nama_karyawan
        } else {
            label_text.text = "\(value)"
        }
        
    }
    
}
