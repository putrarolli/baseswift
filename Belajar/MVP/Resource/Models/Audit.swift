//
//  Audit.swift
//  Belajar
//
//  Created by Ebizu on 10/20/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import ObjectMapper

class Audit: BaseEntity {
    
    var created_by: String?
    var created_date: String?
    var modified_by: String?
    var modified_date: String?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        created_by <- map["created_by"]
        created_date <- map["created_date"]
        modified_by <- map["modified_by"]
        modified_date <- map["modified_date"]
    }
    
}
