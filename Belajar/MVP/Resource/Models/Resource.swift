//
//  Resource.swift
//  Belajar
//
//  Created by Ebizu on 10/20/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import ObjectMapper

class Resource: Audit {
    
    var id_karyawan: String?
    var nama_karyawan: String?
    var nip: String?
    var id_satker: String?
    var email: String?
    var telepon: String?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id_karyawan <- map["id_karyawan"]
        nama_karyawan <- map["nama_karyawan"]
        nip <- map["nip"]
        id_satker <- map["id_satker"]
        email <- map["email"]
        telepon <- map["telepon"]
        nip <- map["nip"]
    }
    
}
