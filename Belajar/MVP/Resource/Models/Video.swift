//
//  Video.swift
//  Belajar
//
//  Created by Ebizu on 10/20/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import ObjectMapper

class Video: BaseEntity  {
    
    var video_id: String?
    var filename: String?
    var title: String?
    var date_created: String?
    var duration: String?
    var status: String?
    var private_val: String?
    var gated: String?
    var competition_id: String?
    var audition_id: String?
    var views: String?
    var properties: VideoProperties?
    var author: Author?
    var image: String?
    var url: String?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        video_id <- map["video_id"]
        filename <- map["filename"]
        title <- map["title"]
        date_created <- map["date_created"]
        duration <- map["duration"]
        status <- map["status"]
        private_val <- map["private_val"]
        gated <- map["gated"]
        competition_id <- map["competition_id"]
        audition_id <- map["audition_id"]
        views <- map["views"]
        properties <- map["properties"]
        author <- map["author"]
        image <- map["image"]
        url <- map["url"]
    }
}

class Author: BaseEntity {
    
    var id: String?
    var type: String?
    var name: String?
    var avatar: String?
    var subscriber: String?
    var subscribe: String?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        name <- map["name"]
        avatar <- map["avatar"]
        subscriber <- map["subscriber"]
        subscribe <- map["subscribe"]
    }
    
}

class VideoProperties: BaseEntity {
    var downloadable: String?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        downloadable <- map["downloadable"]
    }
}
