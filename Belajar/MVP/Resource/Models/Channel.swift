//
//  Channel.swift
//  Belajar
//
//  Created by Zuliadin on 18/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import ObjectMapper

class Channel: BaseEntity {
    
    var id: String?
    var type: String?
    var name: String?
    var owner_id: String?
    var owner_name: String?
    var avatar: String?
    var subscriber: String?
    var subscriber_status: String?
    var videos: String?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        id <- map["id"]
        type <- map["type"]
        name <- map["name"]
        owner_id <- map["owner_id"]
        owner_name <- map["owner_name"]
        avatar <- map["avatar"]
        subscriber <- map["subscriber"]
        subscriber_status <- map["subscriber_status"]
        videos <- map["videos"]
    }
    
}
