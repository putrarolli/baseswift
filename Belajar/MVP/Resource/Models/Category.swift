//
//  Category.swift
//  Belajar
//
//  Created by Zuliadin on 18/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import ObjectMapper

class Category: BaseEntity {
    
    var category_id: String?
    var name: String?
    var image: String?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        category_id <- map["category_id"]
        name <- map["name"]
        image <- map["image"]
    }
}

