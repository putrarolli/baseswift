//
//  ViewController.swift
//  Belajar
//
//  Created by putra rolli on 9/24/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit

class ViewController: UIBaseTableViewController, VideoPresentersDelegate {

    var presenter = VideoPresenters()
    
    override func initProperties() {
        super.initProperties()
        self.registerCell("VideoCell")
        self.addSection("VideoCell", "1")
        self.presenter.delegate = self
        self.presenter.prepareData()
    }
    
    override func onPullRefresh() {
        self.presenter.prepareData()
    }
    
    override func onLoadMoreIndicator() {
        self.presenter.prepareData()
    }
    
    func onSuccessRequest(_ videos: [Video]) {
        self.addSectionItems("1", videos)
        stopAllIndicator()
    }
    
    func onErrorRequest(errorCode: Int, errorMessage: String) {
        showErrorMessageDialog("Mohon maaf lahir batin")
    }
}
