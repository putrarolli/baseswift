//
//  VideoCell.swift
//  Belajar
//
//  Created by Ebizu on 10/20/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit
import AlamofireImage

class VideoCell: UIBaseTableViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var vide_title: UILabel!
    
    class func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupCell(_ value: EntityDelegate, _ indexPath: IndexPath) {
        if let video = value as? Video {
            vide_title.text = "\(indexPath.row) \(video.title!)"
            thumbnail.af_setImage(withURL: URL(string: video.image!)!)
        } else {
            vide_title.text = "\(value)"
        }
    
    }
    
    override func onSelected(_ value: EntityDelegate, _ indexPath: IndexPath) {
        if let video = value as? Video {
            print("click \(video.title!)")
        }
    }
    
}
