//
//  ResourcePresenters.swift
//  Belajar
//
//  Created by Ebizu on 10/20/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

protocol VideoPresentersDelegate: AlmofireServiceDelegate {
    
    func onSuccessRequest(_ resource:[Video])

}

class VideoPresenters: BasePresenter {
    
    var delegate: VideoPresentersDelegate!
    
    private let url = "http://api.metube.co.id/api/video/getlistvideos"
    
    func prepareData() {
        let param: Parameters = [
            "action" : "popularvideos",
            "page" : self.page,
            "limit" : pageLimit()
        ]
        if (!service.onRequest()) {
            service.requestAsync(url, parameters: param, responseSuccess, responseError)
        }
    }
    
    private func responseSuccess(response: ResponseDTO<Video>) {
        print("Success from presenter \(response)")
        if let videos = response.data {
            print("Count \(videos.count)")
            incrementPage()
            delegate.onSuccessRequest(videos)
        }
    }
    
    private func responseError(errorCode: Int, errorMessage: String) {
        print("Error from presenter: \(errorCode) \(errorMessage)")
        delegate.onErrorRequest(errorCode: errorCode, errorMessage: errorMessage)
    }
}
