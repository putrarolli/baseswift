//
//  MainMenuController.swift
//  Belajar
//
//  Created by Ebizu on 10/23/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit

class MainMenuController: UIBaseTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func initView() {
        self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
    }
    
    override func initProperties() {
        super.initProperties()
        self.loadable(false)
        self.registerCell("MainMenuCell")
        self.addSection("MainMenuCell", "1")
        loadMainMenu()
    }
    
    func loadMainMenu() {
        var main_menus = [MainMenu]()
        for i in 0 ... 100 {
            main_menus.append(MainMenu("Menu \(i)"))
        }
        self.addSectionItems("1", main_menus)
    }
    
}
