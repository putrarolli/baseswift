//
//  MainMenu.swift
//  Belajar
//
//  Created by Ebizu on 10/23/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import ObjectMapper

class MainMenu: BaseEntity {
    
    var title: String?
    
    init(_ title: String){
        super.init(map: Map(mappingType: .fromJSON, JSON: [:]))!
        self.title = title
    }
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {}
}
