//
//  MainMenuCell.swift
//  Belajar
//
//  Created by Ebizu on 10/23/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit

class MainMenuCell: UIBaseTableViewCell {

    @IBOutlet weak var menu_title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func setupCell(_ value: EntityDelegate, _ indexPath: IndexPath) {
        if let main_menu = value as? MainMenu {
            menu_title.text = main_menu.title
        }
    }
    
    
    
}
