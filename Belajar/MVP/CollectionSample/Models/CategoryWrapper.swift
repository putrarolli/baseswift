//
//  CategoryWrapper.swift
//  Belajar
//
//  Created by Zuliadin on 18/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import ObjectMapper

class CategoryWrapper: BaseEntity {
    
    var categories:[Category] = [Category]()
    
    override init(){
        super.init()
    }
    
    init(_ category: [Category]){
        super.init()
        categories = category
    }
    
    required init?(map: Map){
        super.init(map: map)
    }
    
}
