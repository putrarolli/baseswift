//
//  ChannelCell.swift
//  Belajar
//
//  Created by Zuliadin on 18/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit

class CategoryCell: UIBaseCollectionViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setupCell(_ value: EntityDelegate) {
        if let category = value as? Category {
            label.text = category.name
        }
    }

}
