//
//  CategoryContainerCell.swift
//  Belajar
//
//  Created by Zuliadin on 18/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import UIKit

class CategoryContainerCell: UIBaseCollectionViewCell {

    let cellCategoryCell :String = "CategoryCell"
    @IBOutlet weak var collectionView: UIBaseCollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // let width = self.contentView.frame.width
        // let height = self.contentView.frame.height
        self.collectionView.layout.scrollDirection = .horizontal
        self.collectionView.cellSize = CGSize(width: self.frame.height, height: self.frame.height)
        self.collectionView.registerCell(cellCategoryCell)
        self.collectionView.addSection(cellCategoryCell, "1")
    }
    
    override func setupCell(_ value: EntityDelegate, _ indexPath: IndexPath) {
        if let categoryWrapper = value as? CategoryWrapper {
            self.collectionView.addSectionItems("1", categoryWrapper.categories)
        }
    }
    
    private func initCell(){
        
        // collectionView.registerCell(cellCategoryCell, "1")
    }

}
