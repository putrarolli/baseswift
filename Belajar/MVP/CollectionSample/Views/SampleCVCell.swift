//
//  SampleCVCell.swift
//  Belajar
//
//  Created by Ebizu on 11/16/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//
import UIKit

class SampleCVCell: UIBaseCollectionViewCell {

    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupCell(_ value: EntityDelegate, _ indexPath: IndexPath) {
        if let video = value as? Video {
            title.text = "\(indexPath.row) \(video.title!)"
        }
    }
    
    override func onSelected(_ value: EntityDelegate, _ indexPath: IndexPath) {
        
    }

}
