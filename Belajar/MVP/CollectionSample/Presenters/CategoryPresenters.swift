//
//  ChannelPresenters.swift
//  Belajar
//
//  Created by Zuliadin on 18/12/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

protocol CategoryPresentersDelegate: AlmofireServiceDelegate {
    
    func onSuccessRequest(_ resource:[Category])
    
}

class CategoryPresenters: BasePresenter {
    
    private let url = "http://clipsapi.metube.co.id/api/category"
    var delegate: CategoryPresentersDelegate?
    
    func prepareData() {
        let param: Parameters = [:
            //"action" : "popularvideos",
            //"page" : self.page,
            //"limit" : pageLimit()
        ]
        if (!service.onRequest()) {
            service.requestAsync(url, parameters: param, responseSuccess, responseError)
        }
    }
    
    private func responseSuccess(response: ResponseDTO<Category>) {
        print("Success from presenter category \(response)")
        if let category = response.data {
            print("Count \(category.count)")
            delegate?.onSuccessRequest(category)
        }
    }
    
    private func responseError(errorCode: Int, errorMessage: String) {
        print("Error from presenter: \(errorCode) \(errorMessage)")
        delegate?.onErrorRequest(errorCode: errorCode, errorMessage: errorMessage)
    }
    
}
