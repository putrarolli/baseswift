//
//  CollectionSampleVC.swift
//  Belajar
//
//  Created by Ebizu on 11/16/17.
//  Copyright © 2017 putra rolli. All rights reserved.
//

import Foundation
import UIKit
class CollectionSampleVC: UIBaseCollectionViewController, CategoryPresentersDelegate, VideoPresentersDelegate, UIBaseCollectionViewDelegate {
    
    let cellCategoryContainer :String = "CategoryContainerCell"
    let cellSample :String = "SampleCVCell"
    var presenterVideo :VideoPresenters = VideoPresenters()
    var presenterCategory :CategoryPresenters = CategoryPresenters()
    
    override func initCells() {
        self.registerCell(cellCategoryContainer)
        self.registerCell(cellSample)
    }
    
    override func initProperties() {
        super.initProperties()
        self.delegateCollection = self
        self.addSection(cellCategoryContainer, "category")
        self.addSection(cellSample, "sample")
        self.presenterVideo.delegate = self
        self.presenterCategory.delegate = self
        self.presenterVideo.prepareData()
        self.presenterCategory.prepareData()
    }
    
    func onSuccessRequest(_ category: [Category]) {
        var cartegoryWrapper = [CategoryWrapper]()
        let cwrapper: CategoryWrapper = CategoryWrapper(category)
        cartegoryWrapper.append(cwrapper)
        self.addSectionItems("category", cartegoryWrapper)
    }
    
    func onSuccessRequest(_ videos: [Video]) {
        self.addSectionItems("sample", videos)
        stopAllIndicator()
    }
    
    func onErrorRequest(errorCode: Int, errorMessage: String) {
        showErrorMessageDialog("Mohon maaf lahir batin")
    }
    
    func sizeForItemAt(indexPath: IndexPath) -> CGSize {
        if 0 == indexPath.section && 0 == indexPath.row {
            return CGSize(width: collectionView.frame.size.width, height: 80)
        } else {
            let width = (collectionView.frame.size.width - 30) / 2
            return CGSize(width: width, height: width)
        }
    }
    
    func insetForSectionAt(section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
}
